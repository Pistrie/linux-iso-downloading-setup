defmodule Lootarr do
  @required_services [
    {:qbittorrent, ~w(puid pgid timezone downloads_dir)a},
    {:prowlarr, ~w(puid pgid timezone)a}
  ]

  @optional_services [
    {:jellyfin, ~w(puid pgid timezone tvshows_dir movies_dir)a},
    {:calibre_web, ~w(puid pgid timezone calibre_dir)a},
    {:sonarr, ~w(puid pgid timezone tvshows_dir downloads_dir)a},
    {:radarr, ~w(puid pgid timezone movies_dir downloads_dir)a},
    {:readarr, ~w(puid pgid timezone books_dir downloads_dir)a},
    {:lidarr, ~w(puid pgid timezone music_dir downloads_dir)a},
    {:kapowarr, ~w(comics_dir downloads_dir)a},
    {:gluetun, ~w(vpn_provider wireguard_private_key wireguard_address timezone)a}
  ]

  # all_options puid pgid timezone downloads_dir tvshows_dir movies_dir calibre_dir books_dir music_dir comics_dir vpn_provider wireguard_private_key wireguard_address

  @options_with_defaults ~w(downloads_dir tvshows_dir movies_dir calibre_dir books_dir music_dir comics_dir)a

  @default_options %{
    downloads_dir: "./downloads",
    tvshows_dir: "./tvshows",
    movies_dir: "./movies",
    calibre_dir: "./calibre",
    books_dir: "./books",
    music_dir: "./music",
    comics_dir: "./comics"
  }

  def main do
    preferences = request_preferences()

    volumes =
      Keyword.merge(@required_services ++ @optional_services, preferences.services)
      |> volumes()

    services =
      Enum.map(preferences.services, fn {service, _} -> apply(Lootarr, service, [preferences]) end)
      |> Enum.join("\n\n")

    # compose = header <> services
    compose = """
    version: "3"

    #{volumes}

    services:
    #{services}
    """

    with :ok <- File.write("/tmp/docker-compose.yml", compose) do
      "Compose file created successfully!"
    else
      {:error, reason} ->
        "⚠️ Unable to write the file. Failed with error code #{reason |> Atom.to_string() |> String.upcase()}"
    end
    |> IO.puts()
  end

  defp request_preferences do
    required_services =
      @required_services

    chosen_services =
      request_services(@optional_services)
      |> Enum.filter(fn {_service, want?} -> want? == true end)
      |> Enum.map(fn {service, _} ->
        Enum.find(@optional_services, fn {opt_service, _config_opts} ->
          opt_service == service
        end)
      end)

    Process.sleep(1000)

    supplied_configuration =
      request_configuration(@required_services ++ chosen_services)
      |> Enum.into(%{})

    Map.put(supplied_configuration, :services, required_services ++ List.wrap(chosen_services))
  end

  defp request_services(services) do
    IO.puts(
      "Listing some services you might want, along with a description of each. Please enter y or n depending on your preferences."
    )

    Enum.map(services, fn {service, _} ->
      service_description(service)
      |> IO.puts()

      {service, get_y_or_n(service)}
    end)
  end

  defp service_description(service) do
    case service do
      :jellyfin ->
        "Jellyfin is a Free Software Media System that manages and streams your media."

      :calibre_web ->
        "Calibre-web is a web app for browsing, reading and downloading eBooks stored in a Calibre database."

      :sonarr ->
        "Sonarr grabs Magnet links or NZB files and sends them to a downloader. It automates the downloading of TV show episodes."

      :radarr ->
        "Radarr grabs Magnet links or NZB files and sends them to a downloader. It automates the downloading of movies."

      :readarr ->
        "Readarr grabs Magnet links or NZB files and sends them to a downloader. It automates the downloading of books."

      :lidarr ->
        "Lidarr grabs Magnet links or NZB files and sends them to a downloader. It automates the downloading of music."

      :kapowarr ->
        "Kapowarr is a software to build and manage a comic book library."

      :gluetun ->
        "Gluetun is a VPN client in a thin Docker container for multiple VPN providers. It hides the Lootarr torrent traffic behind a VPN."
    end
  end

  defp get_y_or_n(service) do
    IO.gets(Atom.to_string(service) <> " (y or n): ")
    |> get_y_or_n(service)
  end

  defp get_y_or_n(answer, _service) when answer == "y\n", do: true
  defp get_y_or_n(answer, _service) when answer == "n\n", do: false

  defp get_y_or_n(_answer, service) do
    IO.gets("Please answer with y or n: ") |> get_y_or_n(service)
  end

  defp request_configuration(services) do
    IO.puts(
      "⚠️ Some configuration options are required. Leave your answer empty if wish to accept the default (if one is shown)"
    )

    distinct_options =
      services
      |> Enum.flat_map(fn {_, options} -> options end)
      |> Enum.uniq()

    if Enum.member?(distinct_options, :vpn_provider) do
      IO.puts(
        "⚠️ Your chosen services include Gluetun. Gluetun configuration can be tricky. Find more info, like available providers, at https://github.com/qdm12/gluetun-wiki/tree/main/setup/providers"
      )
    end

    distinct_options
    |> Enum.map(fn option ->
      case option do
        :puid ->
          {:puid, "What is the PUID of your user? "}

        :pgid ->
          {:pgid, "What is the PGID of your user (usually equal to PUID)? "}

        :timezone ->
          {:timezone, "What is your timezone (e.g. Europe/Amsterdam)? "}

        :downloads_dir ->
          {:downloads_dir,
           "(default #{@default_options.downloads_dir}) Where would you like to store your downloads? "}

        :tvshows_dir ->
          {:tvshows_dir,
           "(default #{@default_options.tvshows_dir}) Where would you like to store your TV shows? "}

        :movies_dir ->
          {:movies_dir,
           "(default #{@default_options.movies_dir}) Where would you like to store your movies? "}

        :calibre_dir ->
          {:calibre_dir,
           "(default #{@default_options.calibre_dir}) Where would you like to store your calibre library? "}

        :books_dir ->
          {:books_dir,
           "(default #{@default_options.books_dir}) Where would you like to store your books? "}

        :music_dir ->
          {:music_dir,
           "(default #{@default_options.music_dir}) Where would you like to store your movies? "}

        :comics_dir ->
          {:comics_dir,
           "(default #{@default_options.comics_dir}) Where would you like to store your comics? "}

        :vpn_provider ->
          {:vpn_provider,
           """
           ⚠️ Available VPN providers can be found at https://github.com/qdm12/gluetun-wiki/tree/main/setup/providers
           ⚠️ Use lowercase characters as your input
           (Gluetun) Which VPN provider do you use?
           """
           |> String.trim_trailing()
           |> Kernel.<>(" ")}

        :wireguard_private_key ->
          {:wireguard_private_key,
           "(Gluetun) Please enter your Wireguard private key from your config file: "}

        :wireguard_address ->
          {:wireguard_address,
           "(Gluetun) Please enter the address from your Wireguard config file: "}
      end
      |> get_config_option()
      |> maybe_apply_default()
    end)
  end

  defp get_config_option({option, prompt}) do
    IO.gets(prompt)
    |> get_config_option(option)
  end

  defp get_config_option(answer, option)
       when answer == "\n" and option in @options_with_defaults do
    {option,
     answer
     |> IO.chardata_to_string()
     |> String.trim()}
  end

  defp get_config_option(answer, option)
       when answer == "\n" and option not in @options_with_defaults do
    IO.gets("This option has no default. Please enter your configuration: ")
    |> get_config_option(option)
  end

  defp get_config_option(answer, option) do
    {option,
     answer
     |> IO.chardata_to_string()
     |> String.trim()}
  end

  defp maybe_apply_default({option, answer} = response) do
    if answer == "" do
      {option, Map.get(@default_options, option)}
    else
      response
    end
  end

  defp volumes(services) do
    EEx.eval_file(
      "./compose-parts/volumes.eex",
      services
    )
    |> String.replace("volumes:\n\n", "volumes:\n")
    |> String.replace("\n\n\n", "\n")
    |> String.trim_trailing()
  end

  @doc false
  def qbittorrent(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/qbittorrent.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def jellyfin(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        tvshows_dir: tvshows_dir,
        movies_dir: movies_dir
      }) do
    EEx.eval_file("./compose-parts/jellyfin.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      tvshows_dir: tvshows_dir,
      movies_dir: movies_dir
    )
  end

  @doc false
  def calibre_web(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        calibre_dir: calibre_dir
      }) do
    EEx.eval_file("./compose-parts/calibre-web.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      calibre_dir: calibre_dir
    )
  end

  @doc false
  def prowlarr(%{
        puid: puid,
        pgid: pgid,
        timezone: tz
      }) do
    EEx.eval_file("./compose-parts/prowlarr.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz
    )
  end

  @doc false
  def sonarr(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        tvshows_dir: tvshows_dir,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/sonarr.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      tvshows_dir: tvshows_dir,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def radarr(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        movies_dir: movies_dir,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/radarr.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      movies_dir: movies_dir,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def readarr(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        books_dir: books_dir,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/readarr.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      books_dir: books_dir,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def lidarr(%{
        puid: puid,
        pgid: pgid,
        timezone: tz,
        music_dir: music_dir,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/lidarr.eex",
      puid: puid,
      pgid: pgid,
      timezone: tz,
      music_dir: music_dir,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def kapowarr(%{
        comics_dir: comics_dir,
        downloads_dir: dl_dir
      }) do
    EEx.eval_file("./compose-parts/kapowarr.eex",
      comics_dir: comics_dir,
      downloads_dir: dl_dir
    )
  end

  @doc false
  def gluetun(%{
        vpn_provider: vpn_provider,
        wireguard_private_key: wireguard_private_key,
        wireguard_address: wireguard_address,
        timezone: tz
      }) do
    EEx.eval_file("./compose-parts/gluetun.eex",
      vpn_provider: vpn_provider,
      wireguard_private_key: wireguard_private_key,
      wireguard_address: wireguard_address,
      timezone: tz
    )
  end
end

Lootarr.main()
